package jagsc.org.abc2017SpringStation;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.AudioManager;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;

import jagsc.org.abc2017SpringStation.resp.Range;
import jagsc.org.abc2017SpringStation.resp.StationResp;
import jagsc.org.abc2017SpringStation.resp.StationWithNameResp;
import jagsc.org.abc2017SpringStation.resp.RankTable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener {

    static String str;
    private GoogleMap mMap;

    private PlaySound PlaySound = new PlaySound();

    private TextView hpText;    //プレイヤーHP表示
    private TextView currentStation;
    private int hp = 100000;    //50から変更
    private TextView turnText;
    private int turn = 10;
    private String cityname="東京";
    private int stationtime = 20;
    private String time;
    private Button firstChoiceButton;
    private Button secondChoiceButton;
    private Button thirdChoiceButton;
    private int citycode1;
    private int citycode2;
    private int citycode3;
    private float lon;
    private float lat;
    private float londiff = 0.003128f;
    private float latdiff = -0.003128f;
    private LatLng latlng;
    //private int k;//検索結果のヒット数

    private int maxHp = hp;

    SQLiteDatabase db;


    private void updateHpTextView() {
        if (turn <= 0) {
            PlaySound.PlayFinish();
            AlertDialog dialog = new AlertDialog.Builder(this)
                    .setTitle("ゲームオーバー")
                    .setMessage("あなたは東京の闇に飲まれました……。\nアプリを終了します。")
                    .setPositiveButton("さようなら", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            final EditText editView = new EditText(MainActivity.this);
                            new AlertDialog.Builder(MainActivity.this)
                                    .setIcon(android.R.drawable.ic_dialog_info)
                                    .setTitle("ユーザ名の登録")
                                    .setView(editView)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int whichButton) {

                                            ScoreCommunicator scorecom = new ScoreCommunicator();

                                            scorecom.registrank(editView.getText().toString(), maxHp,new ScoreCommunicator.RegistRankListener(){
                                                @Override
                                                public void onResponse(RankTable ranktable) {
                                                    //TODO:ランキングテーブルが返ってくるので表示をする(要検討)

                                                }
                                                @Override
                                                public void onFailure(Throwable throwable) {
                                                    Log.e("onFailure", throwable.getMessage());
                                                }

                                            });

                                            //ScoreCommunicator.registrank("kimura", maxHp);
                                            Toast.makeText(MainActivity.this, "ランキング登録しました。スコア:" + maxHp, Toast.LENGTH_LONG).show();
                                            Toast.makeText(MainActivity.this, "バイバイ", Toast.LENGTH_LONG).show();
                                            
                                            finish();
                                        }
                                    })
                                    .setNegativeButton("キャンセル", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int whichButton) {
                                            PlaySound.PlayCancel();
                                            finish();
                                        }
                                    })
                                    .show();

                        }
                    })
                    .setCancelable(false)
                    .create();
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
            PlaySound.StopMusic();
            PlaySound.PlayMusic(this, R.raw.result_bgm);
        } else {
            hpText.setText("SCORE: " + String.valueOf(hp));
            turnText.setText("残りターン: " + String.valueOf(turn));
        }
    }

    private void setStationOnButton(int startStationId){
        cityname = EkiSQLiteWrapper.getStationsName(db,startStationId);
        currentStation.setText(cityname);
        ArrayList<Integer> stationList = EkiSQLiteWrapper.getStationsInRange(db,startStationId,5);
        System.out.println(stationList);
        Collections.shuffle(stationList);
        if(stationList.size() < 3){
            new AlertDialog.Builder(this)
                .setTitle("エラー")
                .setMessage("違う駅を選択してね")
                .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .show();
            db.close();
        }else {
            citycode1 = stationList.get(0);
            citycode2 = stationList.get(1);
            citycode3 = stationList.get(2);
            firstChoiceButton.setText(EkiSQLiteWrapper.getStationsName(db, citycode1));
            secondChoiceButton.setText(EkiSQLiteWrapper.getStationsName(db, citycode2));
            thirdChoiceButton.setText(EkiSQLiteWrapper.getStationsName(db, citycode3));
        }
    }

    private void setMapLocation(int staionId){
        latlng = EkiSQLiteWrapper.getStationsLocation(db,staionId);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latlng, 14));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        // load Player
        ImageView imageView1 = (ImageView) findViewById(R.id.image_player);
        imageView1.setImageResource(R.drawable.player_figure);

        // Buttons
        firstChoiceButton = (Button) findViewById(R.id.first_choice_button);
        secondChoiceButton = (Button) findViewById(R.id.second_choice_button);
        thirdChoiceButton = (Button) findViewById(R.id.third_choice_button);

        // TextView
        // HP Text
        hpText = (TextView) findViewById(R.id.hp_text);
        turnText = (TextView)findViewById(R.id.turn_text);
        updateHpTextView();

        //現在駅を表示するTextView
        currentStation = (TextView) findViewById(R.id.current_station_text);

        //Intentでデータを受け取る
        Intent intent = getIntent();
        cityname = intent.getStringExtra("SName");

        EkiSQLiteOpenHelper sqliteOpenHelper = new EkiSQLiteOpenHelper(getApplicationContext());
        db = sqliteOpenHelper.getReadableDatabase();

        /*駅候補を表示し、選択させる際の処理*/
        try {
            //SQLを叩く(入力された文字を含む駅名一覧をを都道府県名とともに取得するSQL)
            Cursor cursor = db.rawQuery(
                    "SELECT S.station_cd, S.station_name, P.pref_name " +
                    "FROM station AS S " +
                    "INNER JOIN pref AS P ON S.pref_cd = P.pref_cd " +
                    "WHERE S.station_name LIKE '%"+cityname+"%' " +
                    "GROUP BY S.station_g_cd , S.station_name;"
                    ,null);

            //結果をlistに格納する 例:「高尾(東京都)」
            ArrayList nameList = new ArrayList();
            final ArrayList idList = new ArrayList();
            while (cursor.moveToNext()){ //SQLの結果毎にループ
                int station_id = cursor.getInt(0);
                String station_name = cursor.getString(1);
                String pref_name = cursor.getString(2);
                nameList.add(station_name + "(" + pref_name + ")");
                idList.add(station_id);
            }

            if(nameList.size() == 0 ){
                new AlertDialog.Builder(this)
                        .setTitle("エラー")
                        .setMessage("そんな駅ないよ！")
                        .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                                db.close();
                            }
                        })
                        .show();
            }else{
                //可変長配列を固定長配列(final)に変換(わざわざ変換とかしなくても他にいい方法あるかも？)
                final String[] items = (String[]) nameList.toArray(new String[nameList.size()]);

                //選択ダイアログを作成する
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("駅選択");
                builder.setItems( items,new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        setStationOnButton((int)idList.get(which));
                        setMapLocation((int)idList.get(which));
                        db.close();
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        }catch (Exception e) {
            e.printStackTrace();
        }finally {
            //db.close();
        }

        firstChoiceButton.setOnClickListener(this);
        secondChoiceButton.setOnClickListener(this);
        thirdChoiceButton.setOnClickListener(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        try{
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(this,R.raw.transit_map_style)
            );
            if(!success){
                Log.e("onMapReady","Style parsing failed");
            }
        }catch (Resources.NotFoundException e){};
        mMap.setOnMapClickListener(new OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                PlaySound.PlayTap();
                //Toast.makeText(getApplicationContext(), "タップ位置\n緯度：" + latLng.latitude + "\n経度:" + latLng.longitude, Toast.LENGTH_LONG).show();

                // Add a marker in Sydney and move the camera

                CameraUpdate cu = CameraUpdateFactory.newLatLngZoom( new LatLng(35.681167,139.767052), 15);

                //mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
                mMap.moveCamera(cu);

                EkispertWrapper ek = new EkispertWrapper();
                ek.execute();
            }
        });
        //マップの操作を無効にしてチルト操作だけONにすることでonMapClickを実行させる
        UiSettings uiSettings = mMap.getUiSettings();
        uiSettings.setScrollGesturesEnabled(false);
        uiSettings.setRotateGesturesEnabled(false);
        uiSettings.setZoomGesturesEnabled(false);
        uiSettings.setTiltGesturesEnabled(true);

        //lat=Float.valueOf(151);
        //lon=Float.valueOf(45);


        // Add a marker in Sydney and move the camera
        //LatLng sydney = new LatLng(-34, 151);
        //tokyoの座標　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　
        //latlng = new LatLng(35.678083, 139.770444);
        //latlng = new LatLng(lat,lon);
        // mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        //mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latlng, 14));
    }

    @Override
    public void onClick(View v) {
        PlaySound.PlayTap();
        switch (v.getId()) {
            case R.id.first_choice_button:
            case R.id.second_choice_button:
            case R.id.third_choice_button: {
                //Buttonの名前(都市名)をcitynameに代入
                cityname = String.valueOf(((Button) v).getText());
                EkiSpertCommunitator communitator = new EkiSpertCommunitator(MainActivity.this);

                //stationAPIを使用して新しく取得した都市名の座標に地図を移動
                final int code;
                switch (v.getId()) {
                    case R.id.first_choice_button:
                        code = citycode1;
                        break;
                    case R.id.second_choice_button:
                        code = citycode2;
                        break;
                    case R.id.third_choice_button:
                        code = citycode3;
                        break;
                    default:
                        throw new IllegalStateException("bug");
                }

                EkiSQLiteOpenHelper sqliteOpenHelper = new EkiSQLiteOpenHelper(getApplicationContext());
                db = sqliteOpenHelper.getReadableDatabase();
                setStationOnButton(code);
                setMapLocation(code);
                PlaySound.PlayIn_event();

                int change = HitPointEvent.eventOccurs(EkiSQLiteWrapper.getStationsName(db,code), MainActivity.this);
                if(change < 0){
                    PlaySound.PlayIncorrect();
                }else{
                    PlaySound.PlayCorrect();
                }
                hp += change;

                maxHp = Math.max(maxHp, hp);

                turn--;

                updateHpTextView();


                db.close();

                break;
            }
        }
    }
    @Override
    public void onResume(){
        super.onResume();
        PlaySound.initSe(this);
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        if (turn > 0) {
            PlaySound.PlayMusic(this, R.raw.playing_bgm);
        }else{
            PlaySound.PlayMusic(this, R.raw.result_bgm);
        }
    }
    @Override
    public void onPause(){
        super.onPause();
        PlaySound.StopMusic();
        PlaySound.ReleaseSe();
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish();
            return true;
        }
        return false;
    }
    @Override
    public boolean onTouchEvent(MotionEvent event){
        if(event.getAction() == MotionEvent.ACTION_DOWN) {
            PlaySound.PlayTap();
        }
        return super.onTouchEvent(event);
    }
}
